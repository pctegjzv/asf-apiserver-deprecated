package v1alpha1

import metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"

const (
	// ProductName product name
	ProductName = "Alauda ASF"
	// ProjectNamespaceType is a relationship type between both resources
	// currently only "owned" is supported
	MicroservicesEnvironmentKind = "MicroservicesEnvironment"

	// MicroservicesEnvironmentStatusCreating creating
	MicroservicesEnvironmentStatusCreating = "Creating"
	// MicroservicesEnvironmentStatusReady can be reached
	MicroservicesEnvironmentStatusReady = "Ready"
	// MicroservicesEnvironmentStatusError service cannot be reached
	MicroservicesEnvironmentStatusError = "Error"

	// ProjectNamespaceTypeOwned ownership type of relationship
	MicroservicesEnvironmentNamespaceTypeOwned = "owned"
	// ProjectNamespaceStatusUnknown status unknown
	MicroservicesEnvironmentNamespaceStatusUnknown = "Unknown"
	// ProjectNamespaceStatusReady status ready
	MicroservicesEnvironmentNamespaceStatusReady = "Ready"
	// ProjectNamespaceStatusError status error
	MicroservicesEnvironmentNamespaceStatusError = "Error"

	// MicroservicesEnvironmentBindingStatusTypeMicroservicesEnvironment condition type for MicroservicesEnvironmentBinding
	MicroservicesEnvironmentBindingStatusTypeMicroservicesEnvironment = "MicroservicesEnvironment"
	// MicroservicesEnvironmentBindingStatusTypeSecret condition type for Secret

	// For each condition type we have a list of valid reasons:

	// MicroservicesEnvironmentBindingStatusConditionStatusNotFound when a condition type is NotFound
	MicroservicesEnvironmentBindingStatusConditionStatusNotFound = "NotFound"
	// MicroservicesEnvironmentBindingStatusConditionStatusNotValid when a condition type is not filled
	MicroservicesEnvironmentBindingStatusConditionStatusNotValid = "NotValid"
	// MicroservicesEnvironmentBindingStatusConditionStatusReady when a condition type is Found
	MicroservicesEnvironmentBindingStatusConditionStatusReady = "Ready"
	/*




		// RBAC related

		// RoleNameDeveloper  Role for developer-tester
		RoleNameDeveloper = "devops-developer"
		// RoleDisplayNameDeveloper display name for role developer
		RoleDisplayNameDeveloper = "开发测试"
		// RoleDisplayNameDeveloperEn english display name for role developer
		RoleDisplayNameDeveloperEn = "Developer"
		// RoleNameProjectManager Role for project manager
		RoleNameProjectManager = "devops-project-manager"
		// RoleDisplayNameProjectManager display name for ProjectManager
		RoleDisplayNameProjectManager = "项目经理"
		// RoleDisplayNameProjectManagerEn english display name for ProjectManager
		RoleDisplayNameProjectManagerEn = "Project Manager"
		// RoleNamePlatformAdmin ClusterRole for platform admin
		RoleNamePlatformAdmin = "devops-platform-admin"
		// RoleDisplayNamePlatformAdmin display name for Platform Admin
		RoleDisplayNamePlatformAdmin = "平台管理员"
		// RoleDisplayNamePlatformAdminEn english display name for PlatformAdmin
		RoleDisplayNamePlatformAdminEn = "Platform Administrator"
		// RoleNameProjectUser  Role for user
		RoleNameProjectUser = "devops-project-user"
		// RoleDisplayNameProjectUser display name for role project-user
		RoleDisplayNameProjectUser = "项目成员"
		// RoleDisplayNameProjectUserEn english display name for role project-user
		RoleDisplayNameProjectUserEn = "Project Member"
	*/
	// Annotations

	// AnnotationsKeyDisplayName displayName key for annotations
	AnnotationsKeyDisplayName = "alauda.io/displayName"
	// AnnotationsKeyDisplayNameEn english displayName key for annotations
	AnnotationsKeyDisplayNameEn = "alauda.io/displayNameEn"
	// AnnotationsKeyProduct product name key for annotations
	AnnotationsKeyMicroservices = "alauda.io/microservices"
	// AnnotationsKeyProductVersion product version key for annotations
	AnnotationsKeyMicroservicesVersion = "alauda.io/microservicesVersion"
	// AnnotationsKeyRoleVersion role version key for annotations
	AnnotationsKeyRoleVersion = "alauda.io/roleVersion"
	// AnnotationsKeyProject project key for annotations
	AnnotationsKeyMicroservicesEnvironment = "alauda.io/microservicesEnvironment"
	// AnnotationsKeyPipelineLastNumber last number used to generate pipeline store in PipelineConfig
	AnnotationsKeyPipelineLastNumber = "alauda.io/pipeline.last.number"

	// Labels

	// LabelDevopsAlaudaIOKey key used for specific Labels
	LabelDevopsAlaudaIOKey = "devops.alauda.io"
	// LabelDevopsAlaudaIOProjectKey key used for roles that are using in a project
	LabelDevopsAlaudaIOProjectKey = "devops.alauda.io/project"
	// LabelPipelineConfig label key for pipelineConfig
	LabelPipelineConfig = "pipelineConfig"

	// LabelCodeRepoServiceType label key for codeRepoServiceType
	LabelCodeRepoServiceType = "codeRepoServiceType"
	// LabelCodeRepoService label key for codeRepoService
	LabelCodeRepoService = "codeRepoService"
	// LabelCodeRepoBinding label key for codeRepoBinding
	LabelCodeRepoBinding = "codeRepoBinding"
	// LabelCodeRepository label key for codeRepository
	LabelCodeRepository = "codeRepository"

	// TrueString true as string
	TrueString = "true"

	// configmap
	ConfigMapKindName          = "ConfigMap"
	ConfigMapAPIVersion        = "v1"
	SettingsConfigMapNamespace = "alauda-system"
	SettingsConfigMapName      = "devops-config"
	SettingsKeyGithubCreated   = "githubCreated"

	// github
	GithubName = "github"
	GithubHost = "https://api.github.com"
	GithubHtml = "https://github.com"
)

var requestedKeys = []string{AnnotationsKeyMicroservices, AnnotationsKeyMicroservicesVersion}

// IsDevOpsResource returns true for a given ObjectMeta if the resource was created
// by the DevOps Platform
// it will verify some requested keys inside annotations to make sure
func IsASFResource(objMeta metav1.ObjectMeta) bool {
	if len(objMeta.Annotations) > 0 {
		for _, key := range requestedKeys {
			if _, ok := objMeta.Annotations[key]; !ok {
				return false
			}
		}
		return true
	}
	return false
}
