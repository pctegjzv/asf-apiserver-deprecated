/*
Copyright 2018 Alauda.io.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package v1alpha1

import (
	"log"

	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apiserver/pkg/endpoints/request"

	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/util/validation/field"

	"alauda.io/asf-apiserver/pkg/apis/asf"
)

// +genclient
// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object
// +genclient:nonNamespaced

// MicroservicesEnvironment
// +k8s:openapi-gen=true
// +resource:path=microservicesenvironments,strategy=MicroservicesEnvironmentStrategy
type MicroservicesEnvironment struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   MicroservicesEnvironmentSpec   `json:"spec,omitempty"`
	Status MicroservicesEnvironmentStatus `json:"status,omitempty"`
}

// MicroservicesEnvironmentSpec defines the desired state of MicroservicesEnvironment
type MicroservicesEnvironmentSpec struct {
	MicroservicesComponentRefs []MicroservicesComponentRef       `json:"microserviceComponentRefs"`
	Namespace                  *MicroserviceEnvironmentNamespace `json:"namespace"`
}

// ProjectNamespace represents a namespace inside ProjectSpec
type MicroserviceEnvironmentNamespace struct {
	Name string `json:"name"`
	Type string `json:"type"`
}

type MicroservicesComponentRef struct {
	Name   string `json:"name"`
	Status string `json:"status"`
}

// MicroservicesEnvironmentStatus defines the observed state of MicroservicesEnvironment
type MicroservicesEnvironmentStatus struct {
	Status    string                                   `json:"status"`
	Reason    string                                   `json:"reason"`
	Namespace *MicroservicesEnvironmentNamespaceStatus `json:"namespace"`
}

type MicroservicesEnvironmentNamespaceStatus struct {
	Status string `json:"status"`
	Name   string `json:"name"`
}

// Validate checks that an instance of MicroservicesEnvironment is well formed
func (MicroservicesEnvironmentStrategy) Validate(ctx request.Context, obj runtime.Object) field.ErrorList {
	o := obj.(*asf.MicroservicesEnvironment)
	log.Printf("Validating fields for MicroservicesEnvironment %s\n", o.Name)
	errors := field.ErrorList{}
	// perform validation here and add to errors using field.Invalid
	return errors
}

func (MicroservicesEnvironmentStrategy) NamespaceScoped() bool { return false }

func (MicroservicesEnvironmentStatusStrategy) NamespaceScoped() bool { return false }

// DefaultingFunction sets default MicroservicesEnvironment field values
func (MicroservicesEnvironmentSchemeFns) DefaultingFunction(o interface{}) {
	obj := o.(*MicroservicesEnvironment)

	if obj.Spec.MicroservicesComponentRefs == nil || len(obj.Spec.MicroservicesComponentRefs) == 0 {

		obj.Spec.MicroservicesComponentRefs = []MicroservicesComponentRef{
			MicroservicesComponentRef{
				"Zookeeper", "UnCreate",
			},
			MicroservicesComponentRef{
				"Kafka", "UnCreate",
			},
			MicroservicesComponentRef{
				"Config Server", "UnCreate",
			},
			MicroservicesComponentRef{
				"Eureka", "UnCreate",
			},
			MicroservicesComponentRef{
				"Zuul", "UnCreate",
			},
			MicroservicesComponentRef{
				"Turbine", "UnCreate",
			},
			MicroservicesComponentRef{
				"Hystrix DashBoard", "UnCreate",
			},
			MicroservicesComponentRef{
				"Pinpoint - Hbase", "UnCreate",
			},
		}

	}
	// set default field values here
	log.Printf("Defaulting fields for MicroservicesEnvironment %s\n", obj.Name)
}
