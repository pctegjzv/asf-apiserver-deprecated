/*
Copyright 2018 Alauda.io.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package v1alpha1

import (
	"log"

	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apiserver/pkg/endpoints/request"

	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/util/validation/field"

	"alauda.io/asf-apiserver/pkg/apis/asf"
)

// +genclient
// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// MicroservicesComponent
// +k8s:openapi-gen=true
// +resource:path=microservicescomponents,strategy=MicroservicesComponentStrategy
type MicroservicesComponent struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   MicroservicesComponentSpec   `json:"spec,omitempty"`
	Status MicroservicesComponentStatus `json:"status,omitempty"`
}

// MicroservicesComponentSpec defines the desired state of MicroservicesComponent
type MicroservicesComponentSpec struct {
	MicroservicesEnvironmentName string                         `json:"microservicesEnvironmentName"`
	ChartName                    string                         `json:"chartName"`
	ReleaseRef                   MicroservicesComponentInstance `json:"releaseRef"`
}

type MicroservicesComponentInstance struct {
	Name      string `json:"name"`
	Namespace string `json:"namespace"`
}

// MicroservicesComponentStatus defines the observed state of MicroservicesComponent
type MicroservicesComponentStatus struct {
	Installed bool   `json:"installed"`
	Status    string `json:"status"` //error, running, stopped
}

// Validate checks that an instance of MicroservicesComponent is well formed
func (MicroservicesComponentStrategy) Validate(ctx request.Context, obj runtime.Object) field.ErrorList {
	o := obj.(*asf.MicroservicesComponent)
	log.Printf("Validating fields for MicroservicesComponent %s\n", o.Name)
	errors := field.ErrorList{}
	// perform validation here and add to errors using field.Invalid
	return errors
}

// DefaultingFunction sets default MicroservicesComponent field values
func (MicroservicesComponentSchemeFns) DefaultingFunction(o interface{}) {
	obj := o.(*MicroservicesComponent)
	// set default field values here
	log.Printf("Defaulting fields for MicroservicesComponent %s\n", obj.Name)
}
