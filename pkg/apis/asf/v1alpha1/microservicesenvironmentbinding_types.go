/*
Copyright 2018 Alauda.io.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package v1alpha1

import (
	"log"

	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apiserver/pkg/endpoints/request"

	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/util/validation/field"

	"alauda.io/asf-apiserver/pkg/apis/asf"
)

// +genclient
// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// MicroservicesEnvironmentBinding
// +k8s:openapi-gen=true
// +resource:path=microservicesenvironmentbindings,strategy=MicroservicesEnvironmentBindingStrategy
type MicroservicesEnvironmentBinding struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   MicroservicesEnvironmentBindingSpec   `json:"spec,omitempty"`
	Status MicroservicesEnvironmentBindingStatus `json:"status,omitempty"`
}

// MicroservicesEnvironmentBindingSpec defines the desired state of MicroservicesEnvironmentBinding
type MicroservicesEnvironmentBindingSpec struct {
	MicroservicesEnviromentRef MicroservicesEnviromentRef `json:"microservicesEnviroment,omitempty"`
}

type MicroservicesEnviromentRef struct {
	Name      string `json:"name"`
	Namespace string `json:"namespace"`
}

// MicroservicesEnvironmentBindingStatus defines the observed state of MicroservicesEnvironmentBinding
type MicroservicesEnvironmentBindingStatus struct {
	Status     string                                     `json:"status"`
	Conditions []MicroservicesEnvironmentBindingCondition `json:"conditions"`
}

// MicroservicesEnvironmentBindingCondition jenkins binding condition clause
type MicroservicesEnvironmentBindingCondition struct {
	Type        string       `json:"type"`
	LastAttempt *metav1.Time `json:"lastAttempt"`
	Reason      string       `json:"reason,omitempty"`
	Message     string       `json:"message,omitempty"`
	Status      string       `json:"status"`
}

// Validate checks that an instance of MicroservicesEnvironmentBinding is well formed
func (MicroservicesEnvironmentBindingStrategy) Validate(ctx request.Context, obj runtime.Object) field.ErrorList {
	o := obj.(*asf.MicroservicesEnvironmentBinding)
	log.Printf("Validating fields for MicroservicesEnvironmentBinding %s\n", o.Name)
	errors := field.ErrorList{}
	// perform validation here and add to errors using field.Invalid
	return errors
}

// DefaultingFunction sets default MicroservicesEnvironmentBinding field values
func (MicroservicesEnvironmentBindingSchemeFns) DefaultingFunction(o interface{}) {
	obj := o.(*MicroservicesEnvironmentBinding)
	// set default field values here
	log.Printf("Defaulting fields for MicroservicesEnvironmentBinding %s\n", obj.Name)
}
