/*
Copyright 2018 Alauda.io.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

// This file was automatically generated by lister-gen

package internalversion

import (
	asf "alauda.io/asf-apiserver/pkg/apis/asf"
	"k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/labels"
	"k8s.io/client-go/tools/cache"
)

// MicroservicesEnvironmentLister helps list MicroservicesEnvironments.
type MicroservicesEnvironmentLister interface {
	// List lists all MicroservicesEnvironments in the indexer.
	List(selector labels.Selector) (ret []*asf.MicroservicesEnvironment, err error)
	// Get retrieves the MicroservicesEnvironment from the index for a given name.
	Get(name string) (*asf.MicroservicesEnvironment, error)
	MicroservicesEnvironmentListerExpansion
}

// microservicesEnvironmentLister implements the MicroservicesEnvironmentLister interface.
type microservicesEnvironmentLister struct {
	indexer cache.Indexer
}

// NewMicroservicesEnvironmentLister returns a new MicroservicesEnvironmentLister.
func NewMicroservicesEnvironmentLister(indexer cache.Indexer) MicroservicesEnvironmentLister {
	return &microservicesEnvironmentLister{indexer: indexer}
}

// List lists all MicroservicesEnvironments in the indexer.
func (s *microservicesEnvironmentLister) List(selector labels.Selector) (ret []*asf.MicroservicesEnvironment, err error) {
	err = cache.ListAll(s.indexer, selector, func(m interface{}) {
		ret = append(ret, m.(*asf.MicroservicesEnvironment))
	})
	return ret, err
}

// Get retrieves the MicroservicesEnvironment from the index for a given name.
func (s *microservicesEnvironmentLister) Get(name string) (*asf.MicroservicesEnvironment, error) {
	obj, exists, err := s.indexer.GetByKey(name)
	if err != nil {
		return nil, err
	}
	if !exists {
		return nil, errors.NewNotFound(asf.Resource("microservicesenvironment"), name)
	}
	return obj.(*asf.MicroservicesEnvironment), nil
}
