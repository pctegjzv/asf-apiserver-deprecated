/*
Copyright 2018 Alauda.io.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package v1alpha1

import (
	v1alpha1 "alauda.io/asf-apiserver/pkg/apis/asf/v1alpha1"
	scheme "alauda.io/asf-apiserver/pkg/client/clientset_generated/clientset/scheme"
	v1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	types "k8s.io/apimachinery/pkg/types"
	watch "k8s.io/apimachinery/pkg/watch"
	rest "k8s.io/client-go/rest"
)

// MicroservicesEnvironmentsGetter has a method to return a MicroservicesEnvironmentInterface.
// A group's client should implement this interface.
type MicroservicesEnvironmentsGetter interface {
	MicroservicesEnvironments() MicroservicesEnvironmentInterface
}

// MicroservicesEnvironmentInterface has methods to work with MicroservicesEnvironment resources.
type MicroservicesEnvironmentInterface interface {
	Create(*v1alpha1.MicroservicesEnvironment) (*v1alpha1.MicroservicesEnvironment, error)
	Update(*v1alpha1.MicroservicesEnvironment) (*v1alpha1.MicroservicesEnvironment, error)
	UpdateStatus(*v1alpha1.MicroservicesEnvironment) (*v1alpha1.MicroservicesEnvironment, error)
	Delete(name string, options *v1.DeleteOptions) error
	DeleteCollection(options *v1.DeleteOptions, listOptions v1.ListOptions) error
	Get(name string, options v1.GetOptions) (*v1alpha1.MicroservicesEnvironment, error)
	List(opts v1.ListOptions) (*v1alpha1.MicroservicesEnvironmentList, error)
	Watch(opts v1.ListOptions) (watch.Interface, error)
	Patch(name string, pt types.PatchType, data []byte, subresources ...string) (result *v1alpha1.MicroservicesEnvironment, err error)
	MicroservicesEnvironmentExpansion
}

// microservicesEnvironments implements MicroservicesEnvironmentInterface
type microservicesEnvironments struct {
	client rest.Interface
}

// newMicroservicesEnvironments returns a MicroservicesEnvironments
func newMicroservicesEnvironments(c *AsfV1alpha1Client) *microservicesEnvironments {
	return &microservicesEnvironments{
		client: c.RESTClient(),
	}
}

// Get takes name of the microservicesEnvironment, and returns the corresponding microservicesEnvironment object, and an error if there is any.
func (c *microservicesEnvironments) Get(name string, options v1.GetOptions) (result *v1alpha1.MicroservicesEnvironment, err error) {
	result = &v1alpha1.MicroservicesEnvironment{}
	err = c.client.Get().
		Resource("microservicesenvironments").
		Name(name).
		VersionedParams(&options, scheme.ParameterCodec).
		Do().
		Into(result)
	return
}

// List takes label and field selectors, and returns the list of MicroservicesEnvironments that match those selectors.
func (c *microservicesEnvironments) List(opts v1.ListOptions) (result *v1alpha1.MicroservicesEnvironmentList, err error) {
	result = &v1alpha1.MicroservicesEnvironmentList{}
	err = c.client.Get().
		Resource("microservicesenvironments").
		VersionedParams(&opts, scheme.ParameterCodec).
		Do().
		Into(result)
	return
}

// Watch returns a watch.Interface that watches the requested microservicesEnvironments.
func (c *microservicesEnvironments) Watch(opts v1.ListOptions) (watch.Interface, error) {
	opts.Watch = true
	return c.client.Get().
		Resource("microservicesenvironments").
		VersionedParams(&opts, scheme.ParameterCodec).
		Watch()
}

// Create takes the representation of a microservicesEnvironment and creates it.  Returns the server's representation of the microservicesEnvironment, and an error, if there is any.
func (c *microservicesEnvironments) Create(microservicesEnvironment *v1alpha1.MicroservicesEnvironment) (result *v1alpha1.MicroservicesEnvironment, err error) {
	result = &v1alpha1.MicroservicesEnvironment{}
	err = c.client.Post().
		Resource("microservicesenvironments").
		Body(microservicesEnvironment).
		Do().
		Into(result)
	return
}

// Update takes the representation of a microservicesEnvironment and updates it. Returns the server's representation of the microservicesEnvironment, and an error, if there is any.
func (c *microservicesEnvironments) Update(microservicesEnvironment *v1alpha1.MicroservicesEnvironment) (result *v1alpha1.MicroservicesEnvironment, err error) {
	result = &v1alpha1.MicroservicesEnvironment{}
	err = c.client.Put().
		Resource("microservicesenvironments").
		Name(microservicesEnvironment.Name).
		Body(microservicesEnvironment).
		Do().
		Into(result)
	return
}

// UpdateStatus was generated because the type contains a Status member.
// Add a +genclient:noStatus comment above the type to avoid generating UpdateStatus().

func (c *microservicesEnvironments) UpdateStatus(microservicesEnvironment *v1alpha1.MicroservicesEnvironment) (result *v1alpha1.MicroservicesEnvironment, err error) {
	result = &v1alpha1.MicroservicesEnvironment{}
	err = c.client.Put().
		Resource("microservicesenvironments").
		Name(microservicesEnvironment.Name).
		SubResource("status").
		Body(microservicesEnvironment).
		Do().
		Into(result)
	return
}

// Delete takes name of the microservicesEnvironment and deletes it. Returns an error if one occurs.
func (c *microservicesEnvironments) Delete(name string, options *v1.DeleteOptions) error {
	return c.client.Delete().
		Resource("microservicesenvironments").
		Name(name).
		Body(options).
		Do().
		Error()
}

// DeleteCollection deletes a collection of objects.
func (c *microservicesEnvironments) DeleteCollection(options *v1.DeleteOptions, listOptions v1.ListOptions) error {
	return c.client.Delete().
		Resource("microservicesenvironments").
		VersionedParams(&listOptions, scheme.ParameterCodec).
		Body(options).
		Do().
		Error()
}

// Patch applies the patch and returns the patched microservicesEnvironment.
func (c *microservicesEnvironments) Patch(name string, pt types.PatchType, data []byte, subresources ...string) (result *v1alpha1.MicroservicesEnvironment, err error) {
	result = &v1alpha1.MicroservicesEnvironment{}
	err = c.client.Patch(pt).
		Resource("microservicesenvironments").
		SubResource(subresources...).
		Name(name).
		Body(data).
		Do().
		Into(result)
	return
}
