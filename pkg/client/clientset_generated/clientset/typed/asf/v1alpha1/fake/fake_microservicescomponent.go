/*
Copyright 2018 Alauda.io.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package fake

import (
	v1alpha1 "alauda.io/asf-apiserver/pkg/apis/asf/v1alpha1"
	v1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	labels "k8s.io/apimachinery/pkg/labels"
	schema "k8s.io/apimachinery/pkg/runtime/schema"
	types "k8s.io/apimachinery/pkg/types"
	watch "k8s.io/apimachinery/pkg/watch"
	testing "k8s.io/client-go/testing"
)

// FakeMicroservicesComponents implements MicroservicesComponentInterface
type FakeMicroservicesComponents struct {
	Fake *FakeAsfV1alpha1
	ns   string
}

var microservicescomponentsResource = schema.GroupVersionResource{Group: "asf.alauda.io", Version: "v1alpha1", Resource: "microservicescomponents"}

var microservicescomponentsKind = schema.GroupVersionKind{Group: "asf.alauda.io", Version: "v1alpha1", Kind: "MicroservicesComponent"}

// Get takes name of the microservicesComponent, and returns the corresponding microservicesComponent object, and an error if there is any.
func (c *FakeMicroservicesComponents) Get(name string, options v1.GetOptions) (result *v1alpha1.MicroservicesComponent, err error) {
	obj, err := c.Fake.
		Invokes(testing.NewGetAction(microservicescomponentsResource, c.ns, name), &v1alpha1.MicroservicesComponent{})

	if obj == nil {
		return nil, err
	}
	return obj.(*v1alpha1.MicroservicesComponent), err
}

// List takes label and field selectors, and returns the list of MicroservicesComponents that match those selectors.
func (c *FakeMicroservicesComponents) List(opts v1.ListOptions) (result *v1alpha1.MicroservicesComponentList, err error) {
	obj, err := c.Fake.
		Invokes(testing.NewListAction(microservicescomponentsResource, microservicescomponentsKind, c.ns, opts), &v1alpha1.MicroservicesComponentList{})

	if obj == nil {
		return nil, err
	}

	label, _, _ := testing.ExtractFromListOptions(opts)
	if label == nil {
		label = labels.Everything()
	}
	list := &v1alpha1.MicroservicesComponentList{}
	for _, item := range obj.(*v1alpha1.MicroservicesComponentList).Items {
		if label.Matches(labels.Set(item.Labels)) {
			list.Items = append(list.Items, item)
		}
	}
	return list, err
}

// Watch returns a watch.Interface that watches the requested microservicesComponents.
func (c *FakeMicroservicesComponents) Watch(opts v1.ListOptions) (watch.Interface, error) {
	return c.Fake.
		InvokesWatch(testing.NewWatchAction(microservicescomponentsResource, c.ns, opts))

}

// Create takes the representation of a microservicesComponent and creates it.  Returns the server's representation of the microservicesComponent, and an error, if there is any.
func (c *FakeMicroservicesComponents) Create(microservicesComponent *v1alpha1.MicroservicesComponent) (result *v1alpha1.MicroservicesComponent, err error) {
	obj, err := c.Fake.
		Invokes(testing.NewCreateAction(microservicescomponentsResource, c.ns, microservicesComponent), &v1alpha1.MicroservicesComponent{})

	if obj == nil {
		return nil, err
	}
	return obj.(*v1alpha1.MicroservicesComponent), err
}

// Update takes the representation of a microservicesComponent and updates it. Returns the server's representation of the microservicesComponent, and an error, if there is any.
func (c *FakeMicroservicesComponents) Update(microservicesComponent *v1alpha1.MicroservicesComponent) (result *v1alpha1.MicroservicesComponent, err error) {
	obj, err := c.Fake.
		Invokes(testing.NewUpdateAction(microservicescomponentsResource, c.ns, microservicesComponent), &v1alpha1.MicroservicesComponent{})

	if obj == nil {
		return nil, err
	}
	return obj.(*v1alpha1.MicroservicesComponent), err
}

// UpdateStatus was generated because the type contains a Status member.
// Add a +genclient:noStatus comment above the type to avoid generating UpdateStatus().
func (c *FakeMicroservicesComponents) UpdateStatus(microservicesComponent *v1alpha1.MicroservicesComponent) (*v1alpha1.MicroservicesComponent, error) {
	obj, err := c.Fake.
		Invokes(testing.NewUpdateSubresourceAction(microservicescomponentsResource, "status", c.ns, microservicesComponent), &v1alpha1.MicroservicesComponent{})

	if obj == nil {
		return nil, err
	}
	return obj.(*v1alpha1.MicroservicesComponent), err
}

// Delete takes name of the microservicesComponent and deletes it. Returns an error if one occurs.
func (c *FakeMicroservicesComponents) Delete(name string, options *v1.DeleteOptions) error {
	_, err := c.Fake.
		Invokes(testing.NewDeleteAction(microservicescomponentsResource, c.ns, name), &v1alpha1.MicroservicesComponent{})

	return err
}

// DeleteCollection deletes a collection of objects.
func (c *FakeMicroservicesComponents) DeleteCollection(options *v1.DeleteOptions, listOptions v1.ListOptions) error {
	action := testing.NewDeleteCollectionAction(microservicescomponentsResource, c.ns, listOptions)

	_, err := c.Fake.Invokes(action, &v1alpha1.MicroservicesComponentList{})
	return err
}

// Patch applies the patch and returns the patched microservicesComponent.
func (c *FakeMicroservicesComponents) Patch(name string, pt types.PatchType, data []byte, subresources ...string) (result *v1alpha1.MicroservicesComponent, err error) {
	obj, err := c.Fake.
		Invokes(testing.NewPatchSubresourceAction(microservicescomponentsResource, c.ns, name, data, subresources...), &v1alpha1.MicroservicesComponent{})

	if obj == nil {
		return nil, err
	}
	return obj.(*v1alpha1.MicroservicesComponent), err
}
