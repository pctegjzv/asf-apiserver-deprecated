/*
Copyright 2018 Alauda.io.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package fake

import (
	asf "alauda.io/asf-apiserver/pkg/apis/asf"
	v1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	labels "k8s.io/apimachinery/pkg/labels"
	schema "k8s.io/apimachinery/pkg/runtime/schema"
	types "k8s.io/apimachinery/pkg/types"
	watch "k8s.io/apimachinery/pkg/watch"
	testing "k8s.io/client-go/testing"
)

// FakeMicroservicesEnvironmentBindings implements MicroservicesEnvironmentBindingInterface
type FakeMicroservicesEnvironmentBindings struct {
	Fake *FakeAsf
	ns   string
}

var microservicesenvironmentbindingsResource = schema.GroupVersionResource{Group: "asf.alauda.io", Version: "", Resource: "microservicesenvironmentbindings"}

var microservicesenvironmentbindingsKind = schema.GroupVersionKind{Group: "asf.alauda.io", Version: "", Kind: "MicroservicesEnvironmentBinding"}

// Get takes name of the microservicesEnvironmentBinding, and returns the corresponding microservicesEnvironmentBinding object, and an error if there is any.
func (c *FakeMicroservicesEnvironmentBindings) Get(name string, options v1.GetOptions) (result *asf.MicroservicesEnvironmentBinding, err error) {
	obj, err := c.Fake.
		Invokes(testing.NewGetAction(microservicesenvironmentbindingsResource, c.ns, name), &asf.MicroservicesEnvironmentBinding{})

	if obj == nil {
		return nil, err
	}
	return obj.(*asf.MicroservicesEnvironmentBinding), err
}

// List takes label and field selectors, and returns the list of MicroservicesEnvironmentBindings that match those selectors.
func (c *FakeMicroservicesEnvironmentBindings) List(opts v1.ListOptions) (result *asf.MicroservicesEnvironmentBindingList, err error) {
	obj, err := c.Fake.
		Invokes(testing.NewListAction(microservicesenvironmentbindingsResource, microservicesenvironmentbindingsKind, c.ns, opts), &asf.MicroservicesEnvironmentBindingList{})

	if obj == nil {
		return nil, err
	}

	label, _, _ := testing.ExtractFromListOptions(opts)
	if label == nil {
		label = labels.Everything()
	}
	list := &asf.MicroservicesEnvironmentBindingList{}
	for _, item := range obj.(*asf.MicroservicesEnvironmentBindingList).Items {
		if label.Matches(labels.Set(item.Labels)) {
			list.Items = append(list.Items, item)
		}
	}
	return list, err
}

// Watch returns a watch.Interface that watches the requested microservicesEnvironmentBindings.
func (c *FakeMicroservicesEnvironmentBindings) Watch(opts v1.ListOptions) (watch.Interface, error) {
	return c.Fake.
		InvokesWatch(testing.NewWatchAction(microservicesenvironmentbindingsResource, c.ns, opts))

}

// Create takes the representation of a microservicesEnvironmentBinding and creates it.  Returns the server's representation of the microservicesEnvironmentBinding, and an error, if there is any.
func (c *FakeMicroservicesEnvironmentBindings) Create(microservicesEnvironmentBinding *asf.MicroservicesEnvironmentBinding) (result *asf.MicroservicesEnvironmentBinding, err error) {
	obj, err := c.Fake.
		Invokes(testing.NewCreateAction(microservicesenvironmentbindingsResource, c.ns, microservicesEnvironmentBinding), &asf.MicroservicesEnvironmentBinding{})

	if obj == nil {
		return nil, err
	}
	return obj.(*asf.MicroservicesEnvironmentBinding), err
}

// Update takes the representation of a microservicesEnvironmentBinding and updates it. Returns the server's representation of the microservicesEnvironmentBinding, and an error, if there is any.
func (c *FakeMicroservicesEnvironmentBindings) Update(microservicesEnvironmentBinding *asf.MicroservicesEnvironmentBinding) (result *asf.MicroservicesEnvironmentBinding, err error) {
	obj, err := c.Fake.
		Invokes(testing.NewUpdateAction(microservicesenvironmentbindingsResource, c.ns, microservicesEnvironmentBinding), &asf.MicroservicesEnvironmentBinding{})

	if obj == nil {
		return nil, err
	}
	return obj.(*asf.MicroservicesEnvironmentBinding), err
}

// UpdateStatus was generated because the type contains a Status member.
// Add a +genclient:noStatus comment above the type to avoid generating UpdateStatus().
func (c *FakeMicroservicesEnvironmentBindings) UpdateStatus(microservicesEnvironmentBinding *asf.MicroservicesEnvironmentBinding) (*asf.MicroservicesEnvironmentBinding, error) {
	obj, err := c.Fake.
		Invokes(testing.NewUpdateSubresourceAction(microservicesenvironmentbindingsResource, "status", c.ns, microservicesEnvironmentBinding), &asf.MicroservicesEnvironmentBinding{})

	if obj == nil {
		return nil, err
	}
	return obj.(*asf.MicroservicesEnvironmentBinding), err
}

// Delete takes name of the microservicesEnvironmentBinding and deletes it. Returns an error if one occurs.
func (c *FakeMicroservicesEnvironmentBindings) Delete(name string, options *v1.DeleteOptions) error {
	_, err := c.Fake.
		Invokes(testing.NewDeleteAction(microservicesenvironmentbindingsResource, c.ns, name), &asf.MicroservicesEnvironmentBinding{})

	return err
}

// DeleteCollection deletes a collection of objects.
func (c *FakeMicroservicesEnvironmentBindings) DeleteCollection(options *v1.DeleteOptions, listOptions v1.ListOptions) error {
	action := testing.NewDeleteCollectionAction(microservicesenvironmentbindingsResource, c.ns, listOptions)

	_, err := c.Fake.Invokes(action, &asf.MicroservicesEnvironmentBindingList{})
	return err
}

// Patch applies the patch and returns the patched microservicesEnvironmentBinding.
func (c *FakeMicroservicesEnvironmentBindings) Patch(name string, pt types.PatchType, data []byte, subresources ...string) (result *asf.MicroservicesEnvironmentBinding, err error) {
	obj, err := c.Fake.
		Invokes(testing.NewPatchSubresourceAction(microservicesenvironmentbindingsResource, c.ns, name, data, subresources...), &asf.MicroservicesEnvironmentBinding{})

	if obj == nil {
		return nil, err
	}
	return obj.(*asf.MicroservicesEnvironmentBinding), err
}
