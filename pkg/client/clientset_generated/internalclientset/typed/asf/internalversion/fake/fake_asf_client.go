/*
Copyright 2018 Alauda.io.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package fake

import (
	internalversion "alauda.io/asf-apiserver/pkg/client/clientset_generated/internalclientset/typed/asf/internalversion"
	rest "k8s.io/client-go/rest"
	testing "k8s.io/client-go/testing"
)

type FakeAsf struct {
	*testing.Fake
}

func (c *FakeAsf) MicroservicesComponents(namespace string) internalversion.MicroservicesComponentInterface {
	return &FakeMicroservicesComponents{c, namespace}
}

func (c *FakeAsf) MicroservicesEnvironments() internalversion.MicroservicesEnvironmentInterface {
	return &FakeMicroservicesEnvironments{c}
}

func (c *FakeAsf) MicroservicesEnvironmentBindings(namespace string) internalversion.MicroservicesEnvironmentBindingInterface {
	return &FakeMicroservicesEnvironmentBindings{c, namespace}
}

// RESTClient returns a RESTClient that is used to communicate
// with API server by this client implementation.
func (c *FakeAsf) RESTClient() rest.Interface {
	var ret *rest.RESTClient
	return ret
}
