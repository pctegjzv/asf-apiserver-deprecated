/*
Copyright 2018 Alauda.io.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package internalversion

import (
	asf "alauda.io/asf-apiserver/pkg/apis/asf"
	scheme "alauda.io/asf-apiserver/pkg/client/clientset_generated/internalclientset/scheme"
	v1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	types "k8s.io/apimachinery/pkg/types"
	watch "k8s.io/apimachinery/pkg/watch"
	rest "k8s.io/client-go/rest"
)

// MicroservicesEnvironmentBindingsGetter has a method to return a MicroservicesEnvironmentBindingInterface.
// A group's client should implement this interface.
type MicroservicesEnvironmentBindingsGetter interface {
	MicroservicesEnvironmentBindings(namespace string) MicroservicesEnvironmentBindingInterface
}

// MicroservicesEnvironmentBindingInterface has methods to work with MicroservicesEnvironmentBinding resources.
type MicroservicesEnvironmentBindingInterface interface {
	Create(*asf.MicroservicesEnvironmentBinding) (*asf.MicroservicesEnvironmentBinding, error)
	Update(*asf.MicroservicesEnvironmentBinding) (*asf.MicroservicesEnvironmentBinding, error)
	UpdateStatus(*asf.MicroservicesEnvironmentBinding) (*asf.MicroservicesEnvironmentBinding, error)
	Delete(name string, options *v1.DeleteOptions) error
	DeleteCollection(options *v1.DeleteOptions, listOptions v1.ListOptions) error
	Get(name string, options v1.GetOptions) (*asf.MicroservicesEnvironmentBinding, error)
	List(opts v1.ListOptions) (*asf.MicroservicesEnvironmentBindingList, error)
	Watch(opts v1.ListOptions) (watch.Interface, error)
	Patch(name string, pt types.PatchType, data []byte, subresources ...string) (result *asf.MicroservicesEnvironmentBinding, err error)
	MicroservicesEnvironmentBindingExpansion
}

// microservicesEnvironmentBindings implements MicroservicesEnvironmentBindingInterface
type microservicesEnvironmentBindings struct {
	client rest.Interface
	ns     string
}

// newMicroservicesEnvironmentBindings returns a MicroservicesEnvironmentBindings
func newMicroservicesEnvironmentBindings(c *AsfClient, namespace string) *microservicesEnvironmentBindings {
	return &microservicesEnvironmentBindings{
		client: c.RESTClient(),
		ns:     namespace,
	}
}

// Get takes name of the microservicesEnvironmentBinding, and returns the corresponding microservicesEnvironmentBinding object, and an error if there is any.
func (c *microservicesEnvironmentBindings) Get(name string, options v1.GetOptions) (result *asf.MicroservicesEnvironmentBinding, err error) {
	result = &asf.MicroservicesEnvironmentBinding{}
	err = c.client.Get().
		Namespace(c.ns).
		Resource("microservicesenvironmentbindings").
		Name(name).
		VersionedParams(&options, scheme.ParameterCodec).
		Do().
		Into(result)
	return
}

// List takes label and field selectors, and returns the list of MicroservicesEnvironmentBindings that match those selectors.
func (c *microservicesEnvironmentBindings) List(opts v1.ListOptions) (result *asf.MicroservicesEnvironmentBindingList, err error) {
	result = &asf.MicroservicesEnvironmentBindingList{}
	err = c.client.Get().
		Namespace(c.ns).
		Resource("microservicesenvironmentbindings").
		VersionedParams(&opts, scheme.ParameterCodec).
		Do().
		Into(result)
	return
}

// Watch returns a watch.Interface that watches the requested microservicesEnvironmentBindings.
func (c *microservicesEnvironmentBindings) Watch(opts v1.ListOptions) (watch.Interface, error) {
	opts.Watch = true
	return c.client.Get().
		Namespace(c.ns).
		Resource("microservicesenvironmentbindings").
		VersionedParams(&opts, scheme.ParameterCodec).
		Watch()
}

// Create takes the representation of a microservicesEnvironmentBinding and creates it.  Returns the server's representation of the microservicesEnvironmentBinding, and an error, if there is any.
func (c *microservicesEnvironmentBindings) Create(microservicesEnvironmentBinding *asf.MicroservicesEnvironmentBinding) (result *asf.MicroservicesEnvironmentBinding, err error) {
	result = &asf.MicroservicesEnvironmentBinding{}
	err = c.client.Post().
		Namespace(c.ns).
		Resource("microservicesenvironmentbindings").
		Body(microservicesEnvironmentBinding).
		Do().
		Into(result)
	return
}

// Update takes the representation of a microservicesEnvironmentBinding and updates it. Returns the server's representation of the microservicesEnvironmentBinding, and an error, if there is any.
func (c *microservicesEnvironmentBindings) Update(microservicesEnvironmentBinding *asf.MicroservicesEnvironmentBinding) (result *asf.MicroservicesEnvironmentBinding, err error) {
	result = &asf.MicroservicesEnvironmentBinding{}
	err = c.client.Put().
		Namespace(c.ns).
		Resource("microservicesenvironmentbindings").
		Name(microservicesEnvironmentBinding.Name).
		Body(microservicesEnvironmentBinding).
		Do().
		Into(result)
	return
}

// UpdateStatus was generated because the type contains a Status member.
// Add a +genclient:noStatus comment above the type to avoid generating UpdateStatus().

func (c *microservicesEnvironmentBindings) UpdateStatus(microservicesEnvironmentBinding *asf.MicroservicesEnvironmentBinding) (result *asf.MicroservicesEnvironmentBinding, err error) {
	result = &asf.MicroservicesEnvironmentBinding{}
	err = c.client.Put().
		Namespace(c.ns).
		Resource("microservicesenvironmentbindings").
		Name(microservicesEnvironmentBinding.Name).
		SubResource("status").
		Body(microservicesEnvironmentBinding).
		Do().
		Into(result)
	return
}

// Delete takes name of the microservicesEnvironmentBinding and deletes it. Returns an error if one occurs.
func (c *microservicesEnvironmentBindings) Delete(name string, options *v1.DeleteOptions) error {
	return c.client.Delete().
		Namespace(c.ns).
		Resource("microservicesenvironmentbindings").
		Name(name).
		Body(options).
		Do().
		Error()
}

// DeleteCollection deletes a collection of objects.
func (c *microservicesEnvironmentBindings) DeleteCollection(options *v1.DeleteOptions, listOptions v1.ListOptions) error {
	return c.client.Delete().
		Namespace(c.ns).
		Resource("microservicesenvironmentbindings").
		VersionedParams(&listOptions, scheme.ParameterCodec).
		Body(options).
		Do().
		Error()
}

// Patch applies the patch and returns the patched microservicesEnvironmentBinding.
func (c *microservicesEnvironmentBindings) Patch(name string, pt types.PatchType, data []byte, subresources ...string) (result *asf.MicroservicesEnvironmentBinding, err error) {
	result = &asf.MicroservicesEnvironmentBinding{}
	err = c.client.Patch(pt).
		Namespace(c.ns).
		Resource("microservicesenvironmentbindings").
		SubResource(subresources...).
		Name(name).
		Body(data).
		Do().
		Into(result)
	return
}
