/*
Copyright 2018 Alauda.io.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package internalversion

import (
	asf "alauda.io/asf-apiserver/pkg/apis/asf"
	scheme "alauda.io/asf-apiserver/pkg/client/clientset_generated/internalclientset/scheme"
	v1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	types "k8s.io/apimachinery/pkg/types"
	watch "k8s.io/apimachinery/pkg/watch"
	rest "k8s.io/client-go/rest"
)

// MicroservicesComponentsGetter has a method to return a MicroservicesComponentInterface.
// A group's client should implement this interface.
type MicroservicesComponentsGetter interface {
	MicroservicesComponents(namespace string) MicroservicesComponentInterface
}

// MicroservicesComponentInterface has methods to work with MicroservicesComponent resources.
type MicroservicesComponentInterface interface {
	Create(*asf.MicroservicesComponent) (*asf.MicroservicesComponent, error)
	Update(*asf.MicroservicesComponent) (*asf.MicroservicesComponent, error)
	UpdateStatus(*asf.MicroservicesComponent) (*asf.MicroservicesComponent, error)
	Delete(name string, options *v1.DeleteOptions) error
	DeleteCollection(options *v1.DeleteOptions, listOptions v1.ListOptions) error
	Get(name string, options v1.GetOptions) (*asf.MicroservicesComponent, error)
	List(opts v1.ListOptions) (*asf.MicroservicesComponentList, error)
	Watch(opts v1.ListOptions) (watch.Interface, error)
	Patch(name string, pt types.PatchType, data []byte, subresources ...string) (result *asf.MicroservicesComponent, err error)
	MicroservicesComponentExpansion
}

// microservicesComponents implements MicroservicesComponentInterface
type microservicesComponents struct {
	client rest.Interface
	ns     string
}

// newMicroservicesComponents returns a MicroservicesComponents
func newMicroservicesComponents(c *AsfClient, namespace string) *microservicesComponents {
	return &microservicesComponents{
		client: c.RESTClient(),
		ns:     namespace,
	}
}

// Get takes name of the microservicesComponent, and returns the corresponding microservicesComponent object, and an error if there is any.
func (c *microservicesComponents) Get(name string, options v1.GetOptions) (result *asf.MicroservicesComponent, err error) {
	result = &asf.MicroservicesComponent{}
	err = c.client.Get().
		Namespace(c.ns).
		Resource("microservicescomponents").
		Name(name).
		VersionedParams(&options, scheme.ParameterCodec).
		Do().
		Into(result)
	return
}

// List takes label and field selectors, and returns the list of MicroservicesComponents that match those selectors.
func (c *microservicesComponents) List(opts v1.ListOptions) (result *asf.MicroservicesComponentList, err error) {
	result = &asf.MicroservicesComponentList{}
	err = c.client.Get().
		Namespace(c.ns).
		Resource("microservicescomponents").
		VersionedParams(&opts, scheme.ParameterCodec).
		Do().
		Into(result)
	return
}

// Watch returns a watch.Interface that watches the requested microservicesComponents.
func (c *microservicesComponents) Watch(opts v1.ListOptions) (watch.Interface, error) {
	opts.Watch = true
	return c.client.Get().
		Namespace(c.ns).
		Resource("microservicescomponents").
		VersionedParams(&opts, scheme.ParameterCodec).
		Watch()
}

// Create takes the representation of a microservicesComponent and creates it.  Returns the server's representation of the microservicesComponent, and an error, if there is any.
func (c *microservicesComponents) Create(microservicesComponent *asf.MicroservicesComponent) (result *asf.MicroservicesComponent, err error) {
	result = &asf.MicroservicesComponent{}
	err = c.client.Post().
		Namespace(c.ns).
		Resource("microservicescomponents").
		Body(microservicesComponent).
		Do().
		Into(result)
	return
}

// Update takes the representation of a microservicesComponent and updates it. Returns the server's representation of the microservicesComponent, and an error, if there is any.
func (c *microservicesComponents) Update(microservicesComponent *asf.MicroservicesComponent) (result *asf.MicroservicesComponent, err error) {
	result = &asf.MicroservicesComponent{}
	err = c.client.Put().
		Namespace(c.ns).
		Resource("microservicescomponents").
		Name(microservicesComponent.Name).
		Body(microservicesComponent).
		Do().
		Into(result)
	return
}

// UpdateStatus was generated because the type contains a Status member.
// Add a +genclient:noStatus comment above the type to avoid generating UpdateStatus().

func (c *microservicesComponents) UpdateStatus(microservicesComponent *asf.MicroservicesComponent) (result *asf.MicroservicesComponent, err error) {
	result = &asf.MicroservicesComponent{}
	err = c.client.Put().
		Namespace(c.ns).
		Resource("microservicescomponents").
		Name(microservicesComponent.Name).
		SubResource("status").
		Body(microservicesComponent).
		Do().
		Into(result)
	return
}

// Delete takes name of the microservicesComponent and deletes it. Returns an error if one occurs.
func (c *microservicesComponents) Delete(name string, options *v1.DeleteOptions) error {
	return c.client.Delete().
		Namespace(c.ns).
		Resource("microservicescomponents").
		Name(name).
		Body(options).
		Do().
		Error()
}

// DeleteCollection deletes a collection of objects.
func (c *microservicesComponents) DeleteCollection(options *v1.DeleteOptions, listOptions v1.ListOptions) error {
	return c.client.Delete().
		Namespace(c.ns).
		Resource("microservicescomponents").
		VersionedParams(&listOptions, scheme.ParameterCodec).
		Body(options).
		Do().
		Error()
}

// Patch applies the patch and returns the patched microservicesComponent.
func (c *microservicesComponents) Patch(name string, pt types.PatchType, data []byte, subresources ...string) (result *asf.MicroservicesComponent, err error) {
	result = &asf.MicroservicesComponent{}
	err = c.client.Patch(pt).
		Namespace(c.ns).
		Resource("microservicescomponents").
		SubResource(subresources...).
		Name(name).
		Body(data).
		Do().
		Into(result)
	return
}
