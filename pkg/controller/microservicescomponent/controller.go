
/*
Copyright 2018 Alauda.io.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/



package microservicescomponent

import (
	"log"

	"github.com/kubernetes-incubator/apiserver-builder/pkg/builders"

	"alauda.io/asf-apiserver/pkg/apis/asf/v1alpha1"
	"alauda.io/asf-apiserver/pkg/controller/sharedinformers"
	listers "alauda.io/asf-apiserver/pkg/client/listers_generated/asf/v1alpha1"
)

// +controller:group=asf,version=v1alpha1,kind=MicroservicesComponent,resource=microservicescomponents
type MicroservicesComponentControllerImpl struct {
	builders.DefaultControllerFns

	// lister indexes properties about MicroservicesComponent
	lister listers.MicroservicesComponentLister
}

// Init initializes the controller and is called by the generated code
// Register watches for additional resource types here.
func (c *MicroservicesComponentControllerImpl) Init(arguments sharedinformers.ControllerInitArguments) {
	// Use the lister for indexing microservicescomponents labels
	c.lister = arguments.GetSharedInformers().Factory.Asf().V1alpha1().MicroservicesComponents().Lister()
}

// Reconcile handles enqueued messages
func (c *MicroservicesComponentControllerImpl) Reconcile(u *v1alpha1.MicroservicesComponent) error {
	// Implement controller logic here
	log.Printf("Running reconcile MicroservicesComponent for %s\n", u.Name)
	return nil
}

func (c *MicroservicesComponentControllerImpl) Get(namespace, name string) (*v1alpha1.MicroservicesComponent, error) {
	return c.lister.MicroservicesComponents(namespace).Get(name)
}
