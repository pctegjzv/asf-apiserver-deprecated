/*
Copyright 2018 Alauda.io.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package microservicesenvironmentbinding

import (
	"log"
	"time"

	"k8s.io/apimachinery/pkg/util/runtime"

	"alauda.io/asf-apiserver/pkg/client/clientset_generated/clientset"
	"k8s.io/client-go/kubernetes"

	"k8s.io/client-go/tools/record"

	"alauda.io/asf-apiserver/pkg/client/clientset_generated/clientset/scheme"

	"github.com/golang/glog"
	"github.com/kubernetes-incubator/apiserver-builder/pkg/builders"

	asfv1alpha1 "alauda.io/asf-apiserver/pkg/apis/asf/v1alpha1"
	listers "alauda.io/asf-apiserver/pkg/client/listers_generated/asf/v1alpha1"
	"alauda.io/asf-apiserver/pkg/controller/sharedinformers"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	typedcorev1 "k8s.io/client-go/kubernetes/typed/core/v1"
)

const (
	controllerAgentName = "asf-MicroservicesEnvironmentBinding-controller"
	// SuccessSynced is used as part of the Event 'reason' when a Project is synced
	SuccessSynced = "Synced"

	// MessageJenkinsBindingSynced is the message used for an Event fired when a Project
	// is synced successfully
	MessageMicroservicesEnvironmentBindingSynced = "Microservices Environment Binding synced successfully"
)

// +controller:group=asf,version=v1alpha1,kind=MicroservicesEnvironmentBinding,resource=microservicesenvironmentbindings
type MicroservicesEnvironmentBindingControllerImpl struct {
	builders.DefaultControllerFns

	// lister indexes properties about MicroservicesEnvironmentBinding
	lister listers.MicroservicesEnvironmentBindingLister

	msEnvLister listers.MicroservicesEnvironmentLister

	si *sharedinformers.SharedInformers

	kubernetesClientSet *kubernetes.Clientset
	clientSet           clientset.Interface

	// recorder is an event recorder for recording Event resources to the
	// Kubernetes API.
	recorder record.EventRecorder
}

// Init initializes the controller and is called by the generated code
// Register watches for additional resource types here.
func (c *MicroservicesEnvironmentBindingControllerImpl) Init(arguments sharedinformers.ControllerInitArguments) {

	// **** kubernetes resources
	c.si = arguments.GetSharedInformers()
	// Use the lister for indexing microservicesenvironmentbindings labels
	c.lister = arguments.GetSharedInformers().Factory.Asf().V1alpha1().MicroservicesEnvironmentBindings().Lister()
	c.msEnvLister = arguments.GetSharedInformers().Factory.Asf().V1alpha1().MicroservicesEnvironments().Lister()

	clientset, err := clientset.NewForConfig(arguments.GetRestConfig())
	if err != nil {
		glog.Fatalf("error creating machine client: %v", err)
	}
	c.clientSet = clientset
	c.kubernetesClientSet = arguments.GetSharedInformers().KubernetesClientSet

	// setting events
	eventBroadcaster := record.NewBroadcaster()
	eventBroadcaster.StartLogging(glog.Infof)
	eventBroadcaster.StartRecordingToSink(&typedcorev1.EventSinkImpl{Interface: c.si.KubernetesClientSet.CoreV1().Events("")})
	c.recorder = eventBroadcaster.NewRecorder(scheme.Scheme, corev1.EventSource{Component: controllerAgentName})
	// projects

}

// Reconcile handles enqueued messages
func (c *MicroservicesEnvironmentBindingControllerImpl) Reconcile(u *asfv1alpha1.MicroservicesEnvironmentBinding) error {
	// Implement controller logic here
	log.Printf("Running reconcile MicroservicesEnvironmentBinding for %s\n", u.Name)

	var (
		hasErr, shouldSync                   bool
		conditionShouldSync, conditionHasErr bool

		msEnv      *asfv1alpha1.MicroservicesEnvironment
		syncPeriod = time.Minute * 5
		now        = time.Now()
		metaTime   = metav1.NewTime(now)
		resource   metav1.Object
		condition  asfv1alpha1.MicroservicesEnvironmentBindingCondition
		index      int
	)

	if u.Status.Conditions == nil {
		u.Status.Conditions = []asfv1alpha1.MicroservicesEnvironmentBindingCondition{}
	}

	// Verifying MicroservicesEnviroment condition
	// this will get the information for the resource
	// and fill the condition information accordinly
	condition, index, resource, conditionShouldSync, conditionHasErr = checkResourceCondition(
		u.Status.Conditions,
		asfv1alpha1.MicroservicesEnvironmentBindingStatusTypeMicroservicesEnvironment,
		u.Spec.MicroservicesEnviromentRef.Name,
		func() (metav1.Object, error) {
			return c.msEnvLister.Get(u.Spec.MicroservicesEnviromentRef.Name)
		},
		syncPeriod,
	)
	glog.V(7).Infof(
		"MicroservicesEnvironmentBinding[%v] condition result: condition: %v index: %d, resource: %v, shouldSync: %v, hasErr: %v",
		u, condition, index, resource, conditionShouldSync, conditionHasErr,
	)
	if resource != nil {
		msEnv = resource.(*asfv1alpha1.MicroservicesEnvironment)
	}
	shouldSync = shouldSync || conditionShouldSync
	hasErr = hasErr || conditionHasErr
	if conditionShouldSync {
		condition.LastAttempt = &metaTime
		if index < 0 {
			u.Status.Conditions = append(u.Status.Conditions, condition)
		} else {
			u.Status.Conditions[index] = condition
		}
	}

	// if both are not nil we can use the credentials
	// to send a request to the jenkins server
	if msEnv != nil && msEnv.Status.Status == asfv1alpha1.MicroservicesEnvironmentStatusReady {

		u.Status.Status = asfv1alpha1.MicroservicesEnvironmentStatusReady

	} else {
		u.Status.Status = asfv1alpha1.MicroservicesEnvironmentStatusError
	}
	if hasErr || shouldSync {
		_, err := c.clientSet.AsfV1alpha1().MicroservicesEnvironmentBindings(u.GetNamespace()).Update(u)
		if err != nil {
			runtime.HandleError(err)
		} else {
			c.recorder.Event(u, corev1.EventTypeNormal, SuccessSynced, MessageMicroservicesEnvironmentBindingSynced)
		}
	}
	return nil
}

func (c *MicroservicesEnvironmentBindingControllerImpl) Get(namespace, name string) (*asfv1alpha1.MicroservicesEnvironmentBinding, error) {
	return c.lister.MicroservicesEnvironmentBindings(namespace).Get(name)
}

func getCondition(conds []asfv1alpha1.MicroservicesEnvironmentBindingCondition, conditionType string) (cond asfv1alpha1.MicroservicesEnvironmentBindingCondition, index int) {
	index = -1
	cond = asfv1alpha1.MicroservicesEnvironmentBindingCondition{Type: conditionType}
	if len(conds) == 0 {
		return
	}
	for i, x := range conds {
		if x.Type == conditionType {
			cond = x
			index = i
			return
		}
	}
	return
}

func checkResourceCondition(conditions []asfv1alpha1.MicroservicesEnvironmentBindingCondition, conditionType string, resourceName string, getResource func() (metav1.Object, error), syncPeriod time.Duration) (condition asfv1alpha1.MicroservicesEnvironmentBindingCondition, index int, obj metav1.Object, shouldSync, hasErr bool) {
	now := time.Now()
	var err error
	condition, index = getCondition(conditions, conditionType)
	if index < 0 || isExpired(condition.LastAttempt, now, syncPeriod) {
		shouldSync = true
		obj, err = getResource()
		glog.V(7).Infof("MicroservicesEnvironmentBinding[%s] get resource result: %v err %v", resourceName, obj, err)
		if resourceName == "" {
			hasErr = true
			condition.Status = asfv1alpha1.MicroservicesEnvironmentBindingStatusConditionStatusNotValid
			condition.Reason = asfv1alpha1.MicroservicesEnvironmentBindingStatusConditionStatusNotValid
			condition.Message = "Resource name not provided"
		} else if errors.IsNotFound(err) {
			hasErr = true
			condition.Status = asfv1alpha1.MicroservicesEnvironmentBindingStatusConditionStatusNotFound
			condition.Reason = asfv1alpha1.MicroservicesEnvironmentBindingStatusConditionStatusNotFound
			condition.Message = err.Error()
		} else if obj != nil {
			condition.Status = asfv1alpha1.MicroservicesEnvironmentBindingStatusConditionStatusReady
			condition.Reason = ""
			condition.Message = ""
		}
	}
	return
}

func isExpired(last *metav1.Time, now time.Time, period time.Duration) bool {
	return last == nil || last.Add(period).Before(now)
}
