// https://jenkins.io/doc/book/pipeline/syntax/
@Library('alauda-cicd') _

// global variables for pipeline
def GIT_BRANCH
def GIT_COMMIT
// image can be used for promoting...
def IMAGE
def CURRENT_VERSION
def code_data
def DEBUG = false
def deployment
def RELEASE_VERSION
def CHART_VERSION
def RELEASE_BUILD
// def TEST_IMAGE
pipeline {
    // 运行node条件
    // 为了扩容jenkins的功能一般情况会分开一些功能到不同的node上面
    // 这样每个node作用比较清晰，并可以并行处理更多的任务量
    agent {
        label 'all'
    }

    // (optional) 流水线全局设置
    options {
        // 保留多少流水线记录（建议不放在jenkinsfile里面）
        buildDiscarder(logRotator(numToKeepStr: '10'))

        // 不允许并行执行
        disableConcurrentBuilds()
    }
    //(optional) 环境变量
    environment {
      FOLDER = '$GOPATH/src/alauda.io/asf-apiserver'

      // for building an scanning
      REPOSITORY = "asf-apiserver"
      OWNER = "mathildetech"
      // sonar feedback user
      // needs to change together with the credentialsID
      BITBUCKET_FEEDBACK_ACCOUNT = "alaudabot"
      SONARQUBE_BITBUCKET_CREDENTIALS = "alaudabot"
      NAMESPACE = "alauda-system"
      DEPLOYMENT = "asf-apiserver"
      CONTAINER = "backend"
      DINGDING_BOT = "asf-chat-bot"
      TAG_CREDENTIALS = "alaudabot-bitbucket"
    }
    // stages
    stages {
      stage('Checkout') {
        steps {
          script {
              // checkout code
              def scmVars = checkout scm
              // extract git information
              env.GIT_COMMIT = scmVars.GIT_COMMIT
              env.GIT_BRANCH = scmVars.GIT_BRANCH
              GIT_COMMIT = "${scmVars.GIT_COMMIT}"
              GIT_BRANCH = "${scmVars.GIT_BRANCH}"
              RELEASE_VERSION = readFile('.version').trim()
              CHART_VERSION = readFile('.chartversion').trim()
              RELEASE_BUILD = "${RELEASE_VERSION}.${env.BUILD_NUMBER}"
              if (GIT_BRANCH != "master") {
                  def branch = GIT_BRANCH.replace("/","-").replace("_","-")
                  RELEASE_BUILD = "${RELEASE_VERSION}.${branch}.${env.BUILD_NUMBER}"
              }
          }
          // moving project code for the specified folder
          sh """
              rm -rf ${FOLDER}
              mkdir -p ${FOLDER}
              cp -R . ${FOLDER}
              cp -R .git ${FOLDER}/.git
          """
          // installing golang coverage and report tools
          
          sh "go get -u github.com/alauda/gitversion"
          
          script {
              if (GIT_BRANCH == "master") {
                  sh "gitversion patch ${RELEASE_VERSION} > patch"
                  RELEASE_BUILD = readFile("patch").trim()
              }
              echo "release ${RELEASE_VERSION} - release build ${RELEASE_BUILD}"
          }

        }
      }
      stage('CI'){
        parallel {
          stage('Build') {
              steps {
                  script {
                    sh """
                        cd ${FOLDER}
                        go version
                        mkdir bin
                        GO_ENABLED=0 GOOS=linux go build  -v -o ./bin/apiserver cmd/apiserver/main.go
                        GO_ENABLED=0 GOOS=linux go build -v -o ./bin/controller-manager cmd/controller-manager/main.go
                        upx ./bin/apiserver
                        upx ./bin/controller-manager
                        cp artifacts/images/Dockerfile ./bin/
                    """

                    // currently is building code inside the container
                    IMAGE = deploy.dockerBuild(
                        "${FOLDER}/bin/Dockerfile", //Dockerfile
                        "${FOLDER}/bin", // build context
                        "index.alauda.cn/alaudak8s/asf-apiserver", // repo address
                        "${RELEASE_BUILD}", // tag
                        "alaudak8s", // credentials for pushing
                    ).setArg("commit_id", "${GIT_COMMIT}").setArg("app_version", "${RELEASE_BUILD}")
                    // start and push
                    IMAGE.start().push().push(GIT_COMMIT)

                    // build test image
                    // sh "mkdir .tmp && cp artifacts/images/Dockerfile.test .tmp/Dockerfile && cp -R test .tmp"

                    // TEST_IMAGE = deploy.dockerBuild(
                    //     "${FOLDER}/artifacts/images/Dockerfile.test",
                    //     "${FOLDER}",
                    //     "index.alauda.cn/alaudak8s/asf-apiserver-tests",
                    //     "${RELEASE_BUILD}", // tag
                    //     "alaudak8s", // credentials for pushing
                    // )
                    // TEST_IMAGE.start().push()
                  }

              }
          }
          /*
          stage('Code Scan') {
            steps {
                script {
                  // generate reports
                  sh "echo 'sonar.projectVersion=${RELEASE_BUILD}' >> ${FOLDER}/sonar-project.properties"
                  sh "cat ${FOLDER}/sonar-project.properties"
                  sh "cd ${FOLDER} && make check"
                  deploy.scan(
                        REPOSITORY,
                        GIT_BRANCH,
                        SONARQUBE_BITBUCKET_CREDENTIALS,
                        FOLDER,
                        DEBUG,
                        OWNER,
                        BITBUCKET_FEEDBACK_ACCOUNT).start()
                        
                }
            }
          }
          */
        }
      }
      // after build it should start deploying
      stage('Deploying') {
          steps {
              script {
                  // setup kubectl
                  if (GIT_BRANCH == "master") {
                      // master is already merged
                    //   deploy.setupStaging()

                  } else {
                      // pull-requests
                      deploy.setupInt()
                  }
                  // saving current state
                  CURRENT_VERSION = deploy.getDeployment(NAMESPACE, DEPLOYMENT)

                  deployment = deploy.getYaml("./artifacts/deploy/deploy.yaml").getCurrentState()
                  deployment.ignoreContainer("etcd")
                  deployment.setImage(IMAGE.getImage(), "apiserver").setImage(IMAGE.getImage(), "controller")
                   // start deploying
                  deployment.apply()
              }
          }
      }
    //   stage('Test') {
    //       steps {
    //           script {
    //               def integrationContent = readFile "test/integration-tests.yaml"
    //               integrationContent = integrationContent.replace("%TOKEN%", RELEASE_BUILD.replace(".", "-")).replace("%IMAGE%", TEST_IMAGE.getImage())
    //               def testToken = RELEASE_BUILD.replace(".", "-")
    //               writeFile file:"test.yaml", text: integrationContent
    //               def excluded = false
    //               try {
    //                   def tests = deploy.monitorTests(RELEASE_BUILD.replace(".", "-"), ["asf-api"], 20)
    //                   // cleanup test results first
    //                   tests.cleanUp()
                      
    //                   // start tests
    //                   sh "kubectl create -f test.yaml"
    //                   // monitor test results
    //                   tests.monitor()
    //                   // cleanup
    //                   sh "kubectl get pods -n sonobuoy${testToken} --show-all"
    //                   sh "kubectl delete -f test.yaml"
    //                   excluded = true
    //               }
    //               catch (Exception exc) {
    //                   echo "error: ${exc}"
    //                   sh "kubectl get pods -n sonobuoy${testToken} --show-all"
    //                   if (!excluded) {
    //                       // cleanup
    //                       sh "kubectl delete -f test.yaml"
    //                   }
    //                   throw exc
    //               }
    //           }
    //       }
    //   }
      stage('Manual test') {
          when {
              expression {
                // skipping manual test for now
                false
                // GIT_BRANCH == "master"
              }
          }
          steps {
              script {
                  deploy.notificationTest(DEPLOYMENT, DINGDING_BOT, "注意：流水线进入了手工测试阶段了", RELEASE_BUILD)
              }
              input message: 'Approved?'
              
          }
      }
      stage('Promoting') {
          // limit this stage to master only
          when {
              expression {
                  GIT_BRANCH == "master"
              }
          }
          steps {
              script {
                // setup kubectl
                deploy.setupProd()

                // promote to release
                IMAGE.push("release")

                deployment.apply()

                // adding tag to the current commit
                withCredentials([usernamePassword(credentialsId: TAG_CREDENTIALS, passwordVariable: 'GIT_PASSWORD', usernameVariable: 'GIT_USERNAME')]) {
                    sh "git tag -l | xargs git tag -d" // clean local tags
                    sh """
                        git config --global user.email "alaudabot@alauda.io"
                        git config --global user.name "Alauda Bot"
                    """
                    def repo = "https://${GIT_USERNAME}:${GIT_PASSWORD}@bitbucket.org/${OWNER}/${REPOSITORY}.git"
                    sh "git fetch --tags ${repo}" // retrieve all tags
                    sh("git tag -a ${RELEASE_BUILD} -m 'auto add release tag by jenkins'")
                    sh("git push ${repo} --tags")
                }
                build job: '../../charts-pipeline', parameters: [
                  [$class: 'StringParameterValue', name: 'CHART', value: 'asf'],
                  [$class: 'StringParameterValue', name: 'VERSION', value: CHART_VERSION],
                  [$class: 'StringParameterValue', name: 'COMPONENT', value: 'apiserver'],
                  [$class: 'StringParameterValue', name: 'IMAGE_TAG', value: RELEASE_BUILD],
                  [$class: 'BooleanParameterValue', name: 'DEBUG', value: false],
                  [$class: 'StringParameterValue', name: 'ENV', value: ''],
                ], wait: false
              }
          }
      }

    }

    // (optional)
    // happens at the end of the pipeline
    post {
        // 成功
        success {
          echo "Horay!"
          script {
            if (GIT_BRANCH == "master") {
                deploy.notificationSuccess(DEPLOYMENT, DINGDING_BOT, "上线啦！", RELEASE_BUILD)
            } else {
                deploy.notificationSuccess(DEPLOYMENT, DINGDING_BOT, "流水线完成了", RELEASE_BUILD)
            }
          }
        }
        // 失败
        failure {
            echo "damn!"
            // check the npm log
            // fails lets check if it
            script {
                if (deployment != null) {
                  if (GIT_BRANCH == "master") {
                    deploy.setupStaging()
                  } else {
                    deploy.setupInt()
                  }
                  deployment.rollbackConfig().apply()
                  deploy.notificationFailed(DEPLOYMENT, DINGDING_BOT, "流水线失败了", RELEASE_BUILD)
                }
            }
        }
        // 取消的
        aborted {
          echo "aborted!"
        }
    }
}
